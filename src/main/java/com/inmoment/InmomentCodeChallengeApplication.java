package com.inmoment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InmomentCodeChallengeApplication {
	public static void main(String[] args) {
		SpringApplication.run(InmomentCodeChallengeApplication.class, args);
	}
}
