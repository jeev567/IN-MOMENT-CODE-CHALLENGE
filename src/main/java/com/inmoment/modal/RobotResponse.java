package com.inmoment.modal;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * 
 * @author jeev567
 *
 * 
 */
@Entity
public class RobotResponse implements Serializable {
	
	
	private String status;
	private String timeUsed;
	private String timeRemaining;
	private String currentPageIndex;
	private String currentTermIndex;
	@Id
	private String currentTerm;
	@Column(length=3000)
	private String currentTermDefinition;
	private boolean hasNextPage;
	private boolean hasNextTerm;
	private boolean hasPreviousPage;
	private boolean hasPreviousTerm;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTimeUsed() {
		return timeUsed;
	}

	public void setTimeUsed(String timeUsed) {
		this.timeUsed = timeUsed;
	}

	public String getTimeRemaining() {
		return timeRemaining;
	}

	public void setTimeRemaining(String timeRemaining) {
		this.timeRemaining = timeRemaining;
	}

	public String getCurrentPageIndex() {
		return currentPageIndex;
	}

	public void setCurrentPageIndex(String currentPageIndex) {
		this.currentPageIndex = currentPageIndex;
	}

	public String getCurrentTermIndex() {
		return currentTermIndex;
	}

	public void setCurrentTermIndex(String currentTermIndex) {
		this.currentTermIndex = currentTermIndex;
	}

	public String getCurrentTerm() {
		return currentTerm;
	}

	public void setCurrentTerm(String currentTerm) {
		this.currentTerm = currentTerm;
	}

	public String getCurrentTermDefinition() {
		return currentTermDefinition;
	}

	public void setCurrentTermDefinition(String currentTermDefinition) {
		this.currentTermDefinition = currentTermDefinition;
	}

	public boolean getHasNextPage() {
		return hasNextPage;
	}

	public void setHasNextPage(boolean hasNextPage) {
		this.hasNextPage = hasNextPage;
	}

	public boolean getHasNextTerm() {
		return hasNextTerm;
	}

	public void setHasNextTerm(boolean hasNextTerm) {
		this.hasNextTerm = hasNextTerm;
	}

	public boolean getHasPreviousPage() {
		return hasPreviousPage;
	}

	public void setHasPreviousPage(boolean hasPreviousPage) {
		this.hasPreviousPage = hasPreviousPage;
	}

	public boolean getHasPreviousTerm() {
		return hasPreviousTerm;
	}

	public void setHasPreviousTerm(boolean hasPreviousTerm) {
		this.hasPreviousTerm = hasPreviousTerm;
	}

	@Override
	public String toString() {
		return "RobotResponse [status=" + status + ", timeUsed=" + timeUsed + ", timeRemaining=" + timeRemaining
				+ ", currentPageIndex=" + currentPageIndex + ", currentTermIndex=" + currentTermIndex + ", currentTerm="
				+ currentTerm + ", currentTermDefinition=" + currentTermDefinition + ", hasNextPage=" + hasNextPage
				+ ", hasNextTerm=" + hasNextTerm + ", hasPreviousPage=" + hasPreviousPage + ", hasPreviousTerm="
				+ hasPreviousTerm + "]";
	}
}
