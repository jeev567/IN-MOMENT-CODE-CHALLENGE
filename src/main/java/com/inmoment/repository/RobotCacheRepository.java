package com.inmoment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.inmoment.modal.RobotResponse;

@Repository
public interface RobotCacheRepository extends CrudRepository<RobotResponse, String> {
	public RobotResponse findByCurrentTerm(String term);
	
}
