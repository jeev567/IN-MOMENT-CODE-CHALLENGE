package com.inmoment.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@RequestMapping("/")
public class HomeController {
	public String getMeaning(@RequestParam("value") String term) throws JsonParseException, JsonMappingException, IOException {
		return "index.html";
	}
}
