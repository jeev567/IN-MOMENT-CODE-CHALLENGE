package com.inmoment.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.inmoment.service.DictionaryServ;

@RestController
@RequestMapping("/meaning")
public class DictionaryController {

	@Autowired
	private DictionaryServ dictionaryServ;
	
	@RequestMapping(value = "/term", method = RequestMethod.GET)
	public String getMeaning(@RequestParam("value") String term) throws JsonParseException, JsonMappingException, IOException {
		if(term!=null && !term.isEmpty()) {
			return dictionaryServ.getMeaningBeta(term);	
		}
		return "Please Enter A Valid Value";
	}
}
