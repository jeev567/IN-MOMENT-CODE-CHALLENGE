package com.inmoment.service.impl;

import java.io.IOException;

import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.inmoment.modal.RobotResponse;
import com.inmoment.service.RobotServ;

@Component
public class RobotServImpl extends BaseServiceImpl implements RobotServ {
	
	
	
	@Override
	public RobotResponse getRobotStatus() throws JsonParseException, JsonMappingException, IOException {
		RobotResponse response = getRestResponse(STATUS,HttpMethod.GET);
		return response;
	}

	@Override
	public RobotResponse moveToPrevPage()  throws JsonParseException, JsonMappingException, IOException  {
		RobotResponse response = getRestResponse(MOVE_TO_PREV_PAGE,HttpMethod.POST);
		return response;
	}

	@Override
	public RobotResponse moveToNextPage()  throws JsonParseException, JsonMappingException, IOException  {
		RobotResponse response = getRestResponse(MOVE_TO_NEXT_PAGE,HttpMethod.POST);
		return response;
	}

	@Override
	public RobotResponse jumpToFirstPage() throws JsonParseException, JsonMappingException, IOException  {
		RobotResponse response = getRestResponse(JUMP_TO_FIRST_PAGE,HttpMethod.POST);
		return response;
	}

	@Override
	public RobotResponse jumpToLastPage()  throws JsonParseException, JsonMappingException, IOException {
		RobotResponse response = getRestResponse(JUMP_TO_LAST_PAGE,HttpMethod.POST);
		return response;
	}

	@Override
	public RobotResponse moveToNextTerm()  throws JsonParseException, JsonMappingException, IOException {
		RobotResponse response = getRestResponse(MOVE_TO_NEXT_TERM,HttpMethod.POST);
		return response;
	}

	@Override
	public RobotResponse moveToPrevTerm() throws JsonParseException, JsonMappingException, IOException  {
		RobotResponse response = getRestResponse(MOVE_TO_PREV_TERM,HttpMethod.POST);
		return response;
	}

	@Override
	public RobotResponse jumpToFirstTerm() throws JsonParseException, JsonMappingException, IOException  {	
		RobotResponse response = getRestResponse(JUMP_TO_FIRST_TERM,HttpMethod.POST);
		return response;
	}

	@Override
	public RobotResponse jumpToLastTerm() throws JsonParseException, JsonMappingException, IOException  {
		RobotResponse response = getRestResponse(JUMP_TO_LAST_TERM,HttpMethod.POST);
		return response;
	}

}
