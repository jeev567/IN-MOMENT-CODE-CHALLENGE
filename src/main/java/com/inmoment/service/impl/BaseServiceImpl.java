package com.inmoment.service.impl;

import java.io.IOException;
import java.util.Collections;

import org.jvnet.hk2.annotations.Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inmoment.modal.RobotResponse;

@Service
public class BaseServiceImpl {

	@Value("${x-api-key}")
	private String X_API_KEY;
	
	@Value("${STATUS}")
	protected String STATUS;
	@Value("${MOVE_TO_PREV_PAGE}")
	protected String MOVE_TO_PREV_PAGE;
	@Value("${MOVE_TO_NEXT_PAGE}")
	protected String MOVE_TO_NEXT_PAGE;
	@Value("${JUMP_TO_FIRST_PAGE}")
	protected String JUMP_TO_FIRST_PAGE;
	@Value("${JUMP_TO_LAST_PAGE}")
	protected String JUMP_TO_LAST_PAGE;
	@Value("${MOVE_TO_NEXT_TERM}")
	protected String MOVE_TO_NEXT_TERM;
	@Value("${MOVE_TO_PREV_TERM}")
	protected String MOVE_TO_PREV_TERM;
	@Value("${JUMP_TO_FIRST_TERM}")
	protected String JUMP_TO_FIRST_TERM;
	@Value("${JUMP_TO_LAST_TERM}")
	protected String JUMP_TO_LAST_TERM;

	public RobotResponse getRestResponse(String restEndpoints, HttpMethod operation)throws JsonParseException, JsonMappingException, IOException {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders header = new HttpHeaders();
		header.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		header.set("x-api-key", X_API_KEY);
		HttpEntity<String> entity = new HttpEntity<>(header);
		try {
			ResponseEntity<String> body = restTemplate.exchange(restEndpoints, operation, entity, String.class);
			ObjectMapper objectMapper = new ObjectMapper();
			RobotResponse obj = objectMapper.readValue(body.getBody(), RobotResponse.class);
			System.out.println(body.getBody());
			return obj;
		} catch (Exception e) {
			throw e;
		}
		
	}

}
