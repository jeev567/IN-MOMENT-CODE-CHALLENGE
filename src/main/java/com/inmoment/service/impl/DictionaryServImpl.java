package com.inmoment.service.impl;

import java.io.IOException;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.inmoment.modal.RobotResponse;
import com.inmoment.repository.RobotCacheRepository;
import com.inmoment.service.DictionaryServ;
import com.inmoment.service.RobotServ;
/**
 * 
 * @author Jeevan Abraham
 * This is an implementation class of DictionaryServ.
 * The core logic of how to parse the Dictionary is placed here.
 *
 */
@Component
public class DictionaryServImpl implements DictionaryServ {

	@Autowired
	private RobotServ robotServ;
	
	@Autowired
	private RobotCacheRepository repo;
	
	
	
	public String getMeaningBeta(String term) throws JsonParseException, JsonMappingException, IOException {
		//Cache Check
		RobotResponse cache = checkInCache(term);
		if(checkIfContainsNumber(term)) {
			return "Invalid Value";
		}
		if(cache!=null)return cache.getCurrentTermDefinition();
		//Deciding Where to move from
		//Based on first charector the code decides based on ASCII value to traverse from Z to A to A to Z
		RobotResponse current;
		if(term.toLowerCase().charAt(0)>=97 && term.toLowerCase().charAt(0)<=109) {
			current =robotServ.jumpToFirstPage();
			current =robotServ.jumpToLastTerm();
			while(current.getCurrentTerm().compareTo(term)<0) {
			current = robotServ.moveToNextPage();
			}
			current = robotServ.jumpToFirstTerm();
		}else if(term.toLowerCase().charAt(0)>=110 && term.toLowerCase().charAt(0)<=122) {
			//Move from backward
			current =robotServ.jumpToLastPage();
			current =robotServ.jumpToFirstTerm();
			while(current.getCurrentTerm().compareTo(term)>0) {
				current = robotServ.moveToPrevPage();
				}
		}else {
			return "Unknown Value!!";
		}
		
		//current.getCurrentTerm().toLowerCase().charAt(0)<=term.toLowerCase().charAt(0)
		while(current.getHasNextTerm() || current.getCurrentTerm().toLowerCase().compareTo(term.toLowerCase())<=0) {
			repo.save(current);
			if(current.getCurrentTerm().equalsIgnoreCase(term)) {
				 return current.getCurrentTermDefinition();
			 }
			if(!current.getHasNextTerm() && current.getHasNextPage()){
				 current = robotServ.moveToNextPage();
				 current = robotServ.jumpToFirstTerm();
			}
			current = robotServ.moveToNextTerm();
		}
		return "Value not found!!";
	}
	
	


	private boolean checkIfContainsNumber(String term) {
		
		for (int i = 0; i < term.length(); i++) {
			if(term.charAt(i)>='0' && term.charAt(i)<='9') {
				return true;
			}
		}
		return false;
	}




	private RobotResponse checkInCache(String term) {
		RobotResponse cache = repo.findByCurrentTerm(term);
		if(cache!=null)return cache;
		return null;
		
		
	}

	

}
