package com.inmoment.service;

import java.io.IOException;

import org.jvnet.hk2.annotations.Service;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.inmoment.modal.RobotResponse;

@Service
public interface RobotServ {
	
	public RobotResponse getRobotStatus() throws JsonParseException, JsonMappingException, IOException;
	public RobotResponse moveToPrevPage() throws JsonParseException, JsonMappingException, IOException ;
	public RobotResponse moveToNextPage() throws JsonParseException, JsonMappingException, IOException ;
	public RobotResponse jumpToFirstPage()  throws JsonParseException, JsonMappingException, IOException ;
	public RobotResponse jumpToLastPage() throws JsonParseException, JsonMappingException, IOException ;
	public RobotResponse moveToNextTerm() throws JsonParseException, JsonMappingException, IOException ;
	public RobotResponse moveToPrevTerm() throws JsonParseException, JsonMappingException, IOException ;
	public RobotResponse jumpToFirstTerm() throws JsonParseException, JsonMappingException, IOException ;
	public RobotResponse jumpToLastTerm() throws JsonParseException, JsonMappingException, IOException ;
	

}
