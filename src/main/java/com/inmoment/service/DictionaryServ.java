package com.inmoment.service;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public interface DictionaryServ {
		public String getMeaningBeta(String term) throws JsonParseException, JsonMappingException, IOException;
}
